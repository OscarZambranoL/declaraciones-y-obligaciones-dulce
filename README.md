# Obligaciones fiscales y declaraciones

| Nombre |  RFC|
|--|--|
| **Dulce María Castillo León** | CALD9303119V9|
## Obligaciones a presentar
| Obligaciones |  Se presentan de manera:|
|--|--|
| Pago definitivo mensual de IVA **(IVA mensual)** .| Mensual (16 de cada mes) |
|Pago provisional mensual de ISR por servicios profesionales. Régimen de Actividades Empresariales y Profesionales **(ISR mensual)**. |Mensual (16 de cada mes)|
|Declaración informativa mensual de operaciones con terceros de IVA **(IVA informativa terceros)**.|Mensual (16 de cada mes)|
|**Declaración anual de ISR. Personas Físicas**|Anual (Todo el mes de **abril**)|

### Declaraciones

#### 2022

|Año| Mes |IVA mensual |ISR mensual|IVA informativa terceros|
|--|--|--|--|--|
|2022|Enero||||
|2022|Febrero||||
|2022|Marzo||||
|2022|Abril||||
|2022|Mayo||||
|2022|Junio||||
|2022|Julio|||||
|2022|Agosto||||
|2022|Septiembre||||
|2022|Octubre||||
|2022|Noviembre||||
|2022|Diciembre||||


#### 2021

|Año| Mes |IVA mensual |ISR mensual|IVA informativa terceros|
|--|--|--|--|--|
|2021|Enero||||
|2021|Febrero||||
|2021|Marzo||||
|2021|Abril||||
|2021|Mayo||||
|2021|Junio||||
|2021|Julio|||||
|2021|Agosto||||
|2021|Septiembre||||
|2021|Octubre||||
|2021|Noviembre||||
|2021|Diciembre||||


#### 2020

|Año| Mes |IVA mensual |ISR mensual|IVA informativa terceros|
|--|--|--|--|--|
|2020|Enero||||
|2020|Febrero||||
|2020|Marzo||||
|2020|Abril||||
|2020|Mayo||||
|2020|Junio||||
|2020|Julio|||||
|2020|Agosto||||
|2020|Septiembre||||
|2020|Octubre||||
|2020|Noviembre||||
|2020|Diciembre||||

#### 2019

|Año| Mes |IVA mensual |ISR mensual|IVA informativa terceros|
|--|--|--|--|--|
|2019|Enero||||
|2019|Febrero||||
|2019|Marzo||||
|2019|Abril||||
|2019|Mayo||||
|2019|Junio||||
|2019|Julio|||||
|2019|Agosto||||
|2019|Septiembre||||
|2019|Octubre||||
|2019|Noviembre||||
|2019|Diciembre||||


#### 2018

|Año| Mes |IVA mensual |ISR mensual|IVA informativa terceros|
|--|--|--|--|--|
|2018|Enero||||
|2018|Febrero||||
|2018|Marzo||||
|2018|Abril||||
|2018|Mayo||||
|2018|Junio||||
|2018|Julio|||||
|2018|Agosto||||
|2018|Septiembre||||
|2018|Octubre||||
|2018|Noviembre||||
|2018|Diciembre||||
